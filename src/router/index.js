import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import blog from '@/components/blog'
import services from '@/components/services'
import contact from '@/components/contact'
import details from '@/components/details'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/blog',
      name: 'blog',
      component: { template: '<a href="blog.renatolucena.net" v-bind:class="showMore" @click="showMore"> Click Me </a> ' }
    },
    {
      path: '/services',
      name: 'services',
      component: services
    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    },
    {
      path: '/details/:Pid',
      name: 'details',
      component: details
    }
  ]
})
// const app = new Vue({
//   router,
//   template: `
//     <div id="rendered-content">
//       <div>
//         <router-link to="blog.renatolucena.net">blog</router-link>
//       </div>
//       <div>
//         <router-view></router-view>
//       </div>
//     </div>
//   `
// }).$mount('#content');
